package com.sample.lincy.quicknotes.network;

/**
 * The Callback interface is used for asynchronous processing.
 * @param <T> The expected response type
 */
public interface Callback<T> {

    /**
     * Execute the callback with response object.
     *
     * @param response Response object for the callback
     */
    void execute(final T response);
}
