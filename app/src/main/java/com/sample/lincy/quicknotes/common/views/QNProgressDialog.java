package com.sample.lincy.quicknotes.common.views;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.view.Window;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;

import com.sample.lincy.quicknotes.R;

/**
 * Created by karthikrs on 30/10/16.
 */

public class QNProgressDialog extends Dialog
        implements DialogInterface.OnShowListener,
        DialogInterface.OnDismissListener {

    private View mIcon;
    private TextView mText;
    private ObjectAnimator mAnimation;

    public QNProgressDialog (Activity hostActivity)
    {
        super(hostActivity);
        setOwnerActivity(hostActivity);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.qn_progress_dialog);
        setOnShowListener(this);
        setOnDismissListener(this);
        setCancelable(false);
        setCanceledOnTouchOutside(false);

        mIcon = findViewById(R.id.loading_icon);
        mText = (TextView) findViewById(R.id.loading_dialog_message);

        mAnimation = ObjectAnimator.ofFloat(mIcon, "rotation", 0f, 360f);
        mAnimation.setDuration(1500L);
        mAnimation.setRepeatMode(ValueAnimator.RESTART);
        mAnimation.setRepeatCount(ValueAnimator.INFINITE);
        mAnimation.setInterpolator(new LinearInterpolator());
    }

    public void setMessage(String message)
    {
        if(mText != null)
            mText.setText(message);
    }

    @Override
    public void onShow(DialogInterface dialog)
    {
        if(mAnimation != null)
            mAnimation.start();
    }

    @Override
    public void onDismiss(DialogInterface dialog)
    {
        stopAnimation();
    }

    private void stopAnimation()
    {
        if((mAnimation != null) && (mAnimation.isRunning()))
            mAnimation.cancel();
    }

    public void showError(String errorMessage)
    {
        setMessage(errorMessage);
        stopAnimation();
        setCancelable(true);
        setCanceledOnTouchOutside(true);

        if(!isShowing())
            show();
    }

}
