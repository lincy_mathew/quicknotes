package com.sample.lincy.quicknotes.network;

import retrofit.Callback;
import retrofit.RetrofitError;

/**
 * Created by lincym on 31/10/16.
 */

public abstract class RestCallback<T> implements Callback<T> {
    public abstract void failure(RestError restError);

    @Override
    public void failure(RetrofitError error) {
        RestError restError; // create your own class as
        try {
            restError = (RestError) error.getBodyAs(RestError.class);
            // how the error message gonna showup from server side if there is an error
        } catch (Exception ex) {
            restError = new RestError(ex.getMessage());
        }
        if (restError != null)
            failure(restError);
        else {
            failure(new RestError(error.getMessage()));
        }
    }
}