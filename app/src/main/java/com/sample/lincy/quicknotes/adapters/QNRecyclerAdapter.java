package com.sample.lincy.quicknotes.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sample.lincy.quicknotes.R;
import com.sample.lincy.quicknotes.models.QHeader;
import com.sample.lincy.quicknotes.models.QNotes;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by lincym on 31/10/16.
 */

public class QNRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int HEADER = 0;
    private static final int ITEM = 1;
    Context mContext;
    ArrayList<QNotes> listOfNotes;

    public QNRecyclerAdapter(Context mContext, ArrayList<QNotes> listOfNotes) {
        this.mContext = mContext;
        this.listOfNotes = listOfNotes;
    }

    @Override
    public int getItemViewType(int position) {
        if (listOfNotes.get(position) instanceof QHeader)
            return HEADER;
        else
            return ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View convertView;
        QNBaseViewHolder qnViewHolder;

        if (viewType == HEADER) {
            convertView = inflater.inflate(R.layout.cell_notes_header, parent, false);
            qnViewHolder = new QNHeaderViewHolder(convertView);
        } else {
            convertView = inflater.inflate(R.layout.cell_notes_item, parent, false);
            qnViewHolder = new QNItemViewHolder(convertView);
        }

        return qnViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        QNBaseViewHolder viewHolder;
        if (listOfNotes.get(position) instanceof QHeader)
            viewHolder = (QNHeaderViewHolder) holder;
        else
            viewHolder = (QNItemViewHolder) holder;

        viewHolder.bindData(listOfNotes.get(position));
    }

    @Override
    public int getItemCount() {
        return listOfNotes.size();
    }

    public class QNHeaderViewHolder extends QNBaseViewHolder{

        @Bind(R.id.note_category)
        TextView textNoteCategory;

        public QNHeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(QNotes qNotes) {
            textNoteCategory.setText(((QHeader)qNotes).getTitle());
        }
    }

    public class QNItemViewHolder extends QNBaseViewHolder{

        @Bind(R.id.note_title)
        TextView textNoteTitle;
        @Bind(R.id.note_description)
        TextView textNoteDescription;

        public QNItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindData(QNotes qNotes) {
            textNoteTitle.setText(qNotes.getTitle());
            textNoteDescription.setText(qNotes.getContent());
        }

    }

    public abstract class QNBaseViewHolder extends RecyclerView.ViewHolder{

        public QNBaseViewHolder(View itemView) {
            super(itemView);
        }
        public abstract void bindData(QNotes qNotes);

    }

}
