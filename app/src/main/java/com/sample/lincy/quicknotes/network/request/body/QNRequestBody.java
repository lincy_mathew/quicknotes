package com.sample.lincy.quicknotes.network.request.body;

import org.json.*;


public class QNRequestBody {
	
    private String title;
    private String content;
    private double timestamp;
    private String device;
    
    
	public QNRequestBody () {
		
	}	
        
    public QNRequestBody (JSONObject json) {
    
        this.title = json.optString("title");
        this.content = json.optString("content");
        this.timestamp = json.optDouble("timestamp");
        this.device = json.optString("device");

    }
    
    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public double getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(double timestamp) {
        this.timestamp = timestamp;
    }

    public String getDevice() {
        return this.device;
    }

    public void setDevice(String device) {
        this.device = device;
    }


    
}
