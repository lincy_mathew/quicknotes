package com.sample.lincy.quicknotes.network;

import android.content.Context;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sample.lincy.quicknotes.common.AppConstants;
import com.sample.lincy.quicknotes.models.QNotes;
import com.sample.lincy.quicknotes.network.request.body.QNRequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit.client.Response;
import retrofit.mime.TypedInput;

/**
 * Created by lincy on 30/10/16.
 */

public class QNApiManager {
    private static QNApiManager ourInstance;
    Context mContext;

    public QNApiManager(Context context) {
        mContext = context;
    }

    public static QNApiManager getInstance(Context context) {
        if (ourInstance == null) {
            ourInstance = new QNApiManager(context);
        }

        return ourInstance;
    }

    /**
     * Called to add notes to server
     * @param qnRequestBody
     * @param onSuccess
     * @param onFailure
     */
    public void addNotesToServer(QNRequestBody qnRequestBody, final Callback<JSONObject> onSuccess, final Callback<RestError> onFailure) {
        QNApiClient.get(AppConstants.APP_BASE_URL, mContext).addNoteToServer("Basic a2lkX1NraGdsZDFleDo5OTU2ZjhiMWI5ODg0MTQ2OGRhZmJlNDQxY2JmODhhMQ==", "3", qnRequestBody, new RestCallback<Response>() {
            @Override
            public void failure(RestError restError) {
                onFailure.execute(restError);
            }

            @Override
            public void success(Response response, retrofit.client.Response response2) {
                JSONObject jsonObject = convertToJSONObject(response);
                if (jsonObject != null) onSuccess.execute(jsonObject);

            }

        });
    }

    /**
     * Fetches notes from server
     * @param onSuccess
     * @param onFailure
     */
    public void getNotesFromServer(final Callback<QNotes[]> onSuccess, final Callback<RestError> onFailure){
        QNApiClient.get(AppConstants.APP_BASE_URL, mContext).getNotesFromServer("Basic a2lkX1NraGdsZDFleDo5OTU2ZjhiMWI5ODg0MTQ2OGRhZmJlNDQxY2JmODhhMQ==", "3",
                new RestCallback<Response>() {
            @Override
            public void failure(RestError restError) {
                onFailure.execute(restError);
            }

            @Override
            public void success(Response response, Response response2) {
                JSONArray jsonObject = convertToJSONArray(response);
                ObjectMapper mapper = new ObjectMapper();
                mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
                mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
                QNotes[] assetList = null;
                try {
                    assetList = mapper.readValue(String.valueOf(jsonObject), QNotes[].class);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                if (assetList != null) onSuccess.execute(assetList);

            }
        });
    }

    public static JSONObject convertToJSONObject(retrofit.client.Response response) {
        TypedInput body = response.getBody();
        JSONObject jsonObject = null;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(body.in()));
            StringBuilder out = new StringBuilder();
            String newLine = System.getProperty("line.separator");
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
                out.append(newLine);
            }

            // Prints the correct String representation of body.
            System.out.println(out.toString());
            try {
                return new JSONObject(out.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public static JSONArray convertToJSONArray(retrofit.client.Response response) {
        TypedInput body = response.getBody();
        JSONArray jsonObject = null;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(body.in()));
            StringBuilder out = new StringBuilder();
            String newLine = System.getProperty("line.separator");
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
                out.append(newLine);
            }

            // Prints the correct String representation of body.
            System.out.println(out.toString());
            try {
                return new JSONArray(out.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

}
