package com.sample.lincy.quicknotes.network;

import com.sample.lincy.quicknotes.network.request.body.QNRequestBody;

import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;

/**
 * Created by lincym on 31/10/16.
 */

public interface QNApiService {
    @POST("/appdata/kid_Skhgld1ex/notes")
    void addNoteToServer(@Header("Authorization") String authorization, @Header("X-Kinvey-API-Version") String apiVersion, @Body QNRequestBody qnRequestBody, RestCallback<Response> onSuccess);

    @GET("/appdata/kid_Skhgld1ex/notes")
    void getNotesFromServer(@Header("Authorization") String authorization, @Header("X-Kinvey-API-Version") String apiVersion, RestCallback<Response> onSuccess);

}
