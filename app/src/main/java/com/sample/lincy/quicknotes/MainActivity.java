package com.sample.lincy.quicknotes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.sample.lincy.quicknotes.adapters.QNRecyclerAdapter;
import com.sample.lincy.quicknotes.common.AppUtils;
import com.sample.lincy.quicknotes.common.views.QNProgressDialog;
import com.sample.lincy.quicknotes.models.QHeader;
import com.sample.lincy.quicknotes.models.QNotes;
import com.sample.lincy.quicknotes.network.Callback;
import com.sample.lincy.quicknotes.network.QNApiManager;
import com.sample.lincy.quicknotes.network.RestError;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.btn_add_note)
    Button btnAddNote;
    @Bind(R.id.btn_refresh_notes)
    Button btnRefreshNotes;
    @Bind(R.id.listview_notes)
    RecyclerView listOfNotes;

    QNProgressDialog mProgressDialog;
    QNotes[] notesFromServer;
    QNRecyclerAdapter recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        listOfNotes.setHasFixedSize(true);
        listOfNotes.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        fetchNotesFromServer();

        btnAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, QNAddNoteActivity.class);
                startActivity(intent);
            }
        });

        btnRefreshNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fetchNotesFromServer();
            }
        });
    }

    private void fetchNotesFromServer() {

        mProgressDialog = new QNProgressDialog(MainActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setMessage("Fetching notes...");
        mProgressDialog.show();

        QNApiManager.getInstance(MainActivity.this).getNotesFromServer(new Callback<QNotes[]>() {
            @Override
            public void execute(QNotes[] response) {
                notesFromServer = response;
                setServerDataToAdapter(notesFromServer);
                mProgressDialog.dismiss();

            }
        }, new Callback<RestError>() {
            @Override
            public void execute(RestError response) {
                mProgressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Failed to fetch notes", Toast.LENGTH_LONG).show();
            }
        });

    }


    private void setServerDataToAdapter(QNotes[] notes) {
        ArrayList<QNotes> listToAdapter = new ArrayList<>();

        QHeader qHeader1 = new QHeader();
        qHeader1.setTitle(getResources().getString(R.string.text_on_phone));


        QHeader qHeader2 = new QHeader();
        qHeader2.setTitle(getResources().getString(R.string.text_on_server));

        ArrayList<QNotes> privateNotes = new ArrayList<>();
        ArrayList<QNotes> serverNotes = new ArrayList<>();


        for (int i = 0; i < notes.length; i++) {
            if (notes[i].getDevice().equalsIgnoreCase(AppUtils.getDeviceUUID(MainActivity.this)))
                privateNotes.add(notes[i]);
            else
                serverNotes.add(notes[i]);
        }

        if (privateNotes.size() > 0)
            listToAdapter.add(qHeader1);
        listToAdapter.addAll(privateNotes);

        if (serverNotes.size() > 0)
            listToAdapter.add(qHeader2);
        listToAdapter.addAll(serverNotes);

        recyclerAdapter = new QNRecyclerAdapter(MainActivity.this, listToAdapter);
        listOfNotes.setAdapter(recyclerAdapter);
    }
}
