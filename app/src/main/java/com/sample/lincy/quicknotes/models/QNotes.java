package com.sample.lincy.quicknotes.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QNotes implements Serializable {

    @JsonProperty("_id")
    private String id;
    private String content;
    private String device;
    private String title;
    private double timestamp;
    @JsonProperty("_acl")
    private Acl acl;
    @JsonProperty("_kmd")
    private Kmd kmd;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDevice() {
        return this.device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(double timestamp) {
        this.timestamp = timestamp;
    }

    public Acl getAcl() {
        return this.acl;
    }

    public void setAcl(Acl acl) {
        this.acl = acl;
    }

    public Kmd getKmd() {
        return this.kmd;
    }

    public void setKmd(Kmd kmd) {
        this.kmd = kmd;
    }


}
