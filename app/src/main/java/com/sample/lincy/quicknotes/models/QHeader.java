package com.sample.lincy.quicknotes.models;

/**
 * Created by lincym on 31/10/16.
 */

public class QHeader extends QNotes {
    String title;

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }
}
