package com.sample.lincy.quicknotes.network;

import android.content.Context;
import android.util.Log;

import com.sample.lincy.quicknotes.common.AppUtils;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by lincym on 31/10/16.
 */

public class QNApiClient {
    private static QNApiService REST_CLIENT ;

    private static String ROOT = "";
    private static long SIZE_OF_CACHE = 50 * 1024 * 1024; // in MB
    protected static RestAdapter mRestAdapter;
    public static Context mContext;

    public static void setROOT(String ROOT) {
        QNApiClient.ROOT = ROOT;
    }

    public static void setContext(Context context) {
        QNApiClient.mContext = context;
    }

    public static QNApiService get(String url, Context context) {
        setROOT(url);
        setContext(context);
        return setupRestClient();
    }
    private static QNApiService setupRestClient() {

        File httpCacheDirectory = new File(mContext.getCacheDir(), "responses");

        Cache cache = null;
        try {
            cache = new Cache(httpCacheDirectory, SIZE_OF_CACHE);
        } catch (IOException e) {
            Log.e("OKHttp", "Could not create http cache", e);
        }


        // Create OkHttpClient
        OkHttpClient okHttpClient = new OkHttpClient();
//        okHttpClient.setCache(cache);
        okHttpClient.setConnectTimeout(30, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(30, TimeUnit.SECONDS);
        if (cache != null) {
            okHttpClient.setCache(cache);
        }

        mRestAdapter = new RestAdapter.Builder()
                .setEndpoint(ROOT)
                .setClient(new OkClient(okHttpClient))
                .setLogLevel(RestAdapter.LogLevel.NONE)
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("Accept", "application/json;versions=1");
                        if (AppUtils.isNetworkAvailable(mContext)) {
                            int maxAge = 60; // read from cache for 1 minute
                            request.addHeader("Cache-Control", "public, max-age=" + maxAge);
                        } else {
                            int maxStale = 60 * 60 * 24 * 28; // tolerate 4-weeks stale
                            request.addHeader("Cache-Control",
                                    "public, only-if-cached, max-stale=" + maxStale);
                        }
                    }
                })
                .build();




        REST_CLIENT = mRestAdapter.create(QNApiService.class);
        return REST_CLIENT;
    }
}
