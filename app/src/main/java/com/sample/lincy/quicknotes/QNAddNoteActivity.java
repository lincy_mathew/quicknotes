package com.sample.lincy.quicknotes;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sample.lincy.quicknotes.common.AppUtils;
import com.sample.lincy.quicknotes.common.views.QNProgressDialog;
import com.sample.lincy.quicknotes.network.Callback;
import com.sample.lincy.quicknotes.network.QNApiManager;
import com.sample.lincy.quicknotes.network.RestError;
import com.sample.lincy.quicknotes.network.request.body.QNRequestBody;

import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by lincy on 30/10/16.
 */

public class QNAddNoteActivity extends Activity {

    @Bind(R.id.edittext_note_title)
    EditText editTextNoteTitle;
    @Bind(R.id.edittext_note_description)
    EditText getEditTextNoteDescription;
    @Bind(R.id.btn_save_note)
    Button btnSaveNote;

    QNProgressDialog mProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);
        ButterKnife.bind(this);

        btnSaveNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendNoteToServer();
            }
        });
    }

    private void sendNoteToServer() {

        QNRequestBody qnRequestBody = new QNRequestBody();
        qnRequestBody.setTitle(editTextNoteTitle.getText().toString().trim());
        qnRequestBody.setContent(getEditTextNoteDescription.getText().toString().trim());
        qnRequestBody.setTimestamp(AppUtils.getTimestamp());
        qnRequestBody.setDevice(AppUtils.getDeviceUUID(QNAddNoteActivity.this));

        mProgressDialog = new QNProgressDialog(QNAddNoteActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setMessage("Adding note...");
        mProgressDialog.show();
        QNApiManager.getInstance(QNAddNoteActivity.this).addNotesToServer(qnRequestBody, new Callback<JSONObject>() {
            @Override
            public void execute(JSONObject response) {
                mProgressDialog.dismiss();
                finish();
                Toast.makeText(QNAddNoteActivity.this, "Successfully added note to server", Toast.LENGTH_LONG).show();
            }
        }, new Callback<RestError>() {
            @Override
            public void execute(RestError response) {
                mProgressDialog.dismiss();
                Toast.makeText(QNAddNoteActivity.this, "Failure in adding note to server", Toast.LENGTH_LONG).show();
            }
        });

    }
}
