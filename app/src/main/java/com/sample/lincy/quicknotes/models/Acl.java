package com.sample.lincy.quicknotes.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.json.*;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Acl implements Serializable {
	
    private String creator;
    
    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }


    
}
