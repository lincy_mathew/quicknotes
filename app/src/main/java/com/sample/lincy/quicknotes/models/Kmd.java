package com.sample.lincy.quicknotes.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Kmd implements Serializable {

    private String lmt;
    private String ect;

    public String getLmt() {
        return this.lmt;
    }

    public void setLmt(String lmt) {
        this.lmt = lmt;
    }

    public String getEct() {
        return this.ect;
    }

    public void setEct(String ect) {
        this.ect = ect;
    }


}
